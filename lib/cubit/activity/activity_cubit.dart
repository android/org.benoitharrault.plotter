import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:plotter/models/activity/activity.dart';

part 'activity_state.dart';

class ActivityCubit extends HydratedCubit<ActivityState> {
  ActivityCubit()
      : super(ActivityState(
          currentActivity: Activity.createNull(),
        ));

  void updateState(Activity activity) {
    emit(ActivityState(
      currentActivity: activity,
    ));
  }

  void refresh() {
    final Activity activity = Activity(
      // Settings
      activitySettings: state.currentActivity.activitySettings,
      // State
      isRunning: state.currentActivity.isRunning,
      // Base data
      apiHost: state.currentActivity.apiHost,
      // Activity data
      apiStatus: state.currentActivity.apiStatus,
    );
    // activity.dump();

    updateState(activity);
  }

  void startNewActivity(BuildContext context) {
    final ActivitySettingsCubit activitySettingsCubit =
        BlocProvider.of<ActivitySettingsCubit>(context);

    final Activity newActivity = Activity.createNew(
      // Settings
      activitySettings: activitySettingsCubit.state.settings,
    );

    newActivity.dump();

    updateState(newActivity);
    refresh();
  }

  bool canBeResumed() {
    return state.currentActivity.canBeResumed;
  }

  void quitActivity() {
    state.currentActivity.isRunning = false;
    refresh();
  }

  void resumeSavedActivity() {
    state.currentActivity.isRunning = true;
    refresh();
  }

  void deleteSavedActivity() {
    state.currentActivity.isRunning = false;
    refresh();
  }

  void setApiHost(String apiHost) {
    state.currentActivity.apiHost = apiHost;
    refresh();
  }

  void updateApiStatus(String apiStatus) {
    state.currentActivity.apiStatus = apiStatus;
    refresh();
  }

  @override
  ActivityState? fromJson(Map<String, dynamic> json) {
    final Activity currentActivity = json['currentActivity'] as Activity;

    return ActivityState(
      currentActivity: currentActivity,
    );
  }

  @override
  Map<String, dynamic>? toJson(ActivityState state) {
    return <String, dynamic>{
      'currentActivity': state.currentActivity.toJson(),
    };
  }
}
