import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:plotter/cubit/activity/activity_cubit.dart';
import 'package:plotter/models/activity/activity.dart';

class Api {
  static Future<void> updateApiStatus(ActivityCubit activityCubit) async {
    final Activity currentActivity = activityCubit.state.currentActivity;
    final response = await http.get(Uri.http(currentActivity.apiHost, 'status'));

    String responseData = utf8.decode(response.bodyBytes);
    Map<String, dynamic> status = json.decode(responseData);

    activityCubit.updateApiStatus(status['status']);
  }

  static Future<void> move(ActivityCubit activityCubit, String direction) async {
    final Activity currentActivity = activityCubit.state.currentActivity;

    final response = await http.get(Uri.http(currentActivity.apiHost, direction));

    String responseData = utf8.decode(response.bodyBytes);
    Map<String, dynamic> result = json.decode(responseData);

    activityCubit.updateApiStatus(result['result']);
  }
}
