import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:plotter/cubit/activity/activity_cubit.dart';
import 'package:plotter/models/activity/activity.dart';
import 'package:plotter/ui/widgets/keyboard.dart';
import 'package:plotter/utils/api.dart';

class PageRemoteControl extends StatelessWidget {
  const PageRemoteControl({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
        child: Column(
          children: [
            const Keyboard(),
            TextButton(
              child: const Text('get API status'),
              onPressed: () {
                final ActivityCubit activityCubit = BlocProvider.of<ActivityCubit>(context);

                Api.updateApiStatus(activityCubit);
              },
            ),
            BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                final Activity currentActivity = activityState.currentActivity;

                return Text(currentActivity.apiStatus);
              },
            )
          ],
        ),
      ),
    );
  }
}
