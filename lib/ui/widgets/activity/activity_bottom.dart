import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:plotter/cubit/activity/activity_cubit.dart';
import 'package:plotter/models/activity/activity.dart';

class ActivityBottomWidget extends StatelessWidget {
  const ActivityBottomWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return Text(currentActivity.apiHost.toString());
      },
    );
  }
}
