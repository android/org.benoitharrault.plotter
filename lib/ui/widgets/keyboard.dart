import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:plotter/cubit/activity/activity_cubit.dart';
import 'package:plotter/utils/api.dart';

class Keyboard extends StatelessWidget {
  const Keyboard({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 2),
      padding: const EdgeInsets.all(2),
      child: Table(
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        children: [
          TableRow(
            children: [
              TableCell(child: buildKeyWidget(context, '')),
              TableCell(child: buildKeyWidget(context, 'north')),
              TableCell(child: buildKeyWidget(context, '')),
            ],
          ),
          TableRow(
            children: [
              TableCell(child: buildKeyWidget(context, 'west')),
              TableCell(child: buildKeyWidget(context, '')),
              TableCell(child: buildKeyWidget(context, 'east')),
            ],
          ),
          TableRow(
            children: [
              TableCell(child: buildKeyWidget(context, '')),
              TableCell(child: buildKeyWidget(context, 'south')),
              TableCell(child: buildKeyWidget(context, '')),
            ],
          ),
        ],
      ),
    );
  }

  Widget buildKeyWidget(BuildContext context, String direction) {
    String keyText = '';

    String north = '🔼';
    String south = '🔽';
    String west = '◀️';
    String east = '▶️';

    if (direction == 'north') {
      keyText = north;
    } else {
      if (direction == 'south') {
        keyText = south;
      } else {
        if (direction == 'west') {
          keyText = west;
        } else {
          if (direction == 'east') {
            keyText = east;
          }
        }
      }
    }

    return TextButton(
      style: TextButton.styleFrom(
        padding: const EdgeInsets.all(0),
      ),
      child: Text(keyText,
          style: const TextStyle(
            fontSize: 60.0,
            fontWeight: FontWeight.w800,
          ),
          textAlign: TextAlign.center),
      onPressed: () {
        if (keyText != '') {
          final ActivityCubit activityCubit = BlocProvider.of<ActivityCubit>(context);

          Api.move(activityCubit, direction);
        }
      },
    );
  }
}
