import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:plotter/cubit/activity/activity_cubit.dart';

import 'package:plotter/ui/pages/home.dart';
import 'package:plotter/ui/pages/plotter.dart';
import 'package:plotter/ui/pages/remote_control.dart';

class ApplicationConfig {
  // plotter type
  static const String parameterCodePlotterType = 'plotterType';
  static const String plotterTypeNone = 'none';
  static const String plotterTypeRadial = 'radial';
  static const String plotterTypeAxial = 'axial';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexPlotter = 1;
  static const int activityPageIndexRemoteControl = 2;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Stepper plotter assistant',
    activitySettings: [
      // plotter type
      ApplicationSettingsParameter(
        code: parameterCodePlotterType,
        values: [
          ApplicationSettingsParameterItemValue(
            value: plotterTypeNone,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: plotterTypeRadial,
          ),
          ApplicationSettingsParameterItemValue(
            value: plotterTypeAxial,
          ),
        ],
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexPlotter);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexPlotter);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      displayBottomNavBar: true,
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageHome();
              },
            );
          },
        ),
        activityPageIndexPlotter: ActivityPageItem(
          code: 'page_plotter',
          icon: Icon(UniconsLine.robot),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PagePlotter();
              },
            );
          },
        ),
        activityPageIndexRemoteControl: ActivityPageItem(
          code: 'page_remote_control',
          icon: Icon(UniconsLine.pen),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageRemoteControl();
              },
            );
          },
        ),
      },
    ),
  );
}
