import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:plotter/config/application_config.dart';

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,

    // Base data
    this.apiHost = '',

    // Activity data
    this.apiStatus = '',
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;

  // Base data
  String apiHost;

  // Activity data
  String apiStatus;

  factory Activity.createNull() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      apiHost: '',
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isRunning: true,
      // Base data
      apiHost: '',
    );
  }

  bool get canBeResumed => false;

  void dump() {
    printlog('');
    printlog('## Current activity dump:');
    printlog('');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('  Base data');
    printlog('    apiHost: $apiHost');
    printlog('  Activity data');
    printlog('    apiStatus: $apiStatus');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      // Base data
      'apiHost': apiHost,
      // Activity data
      'apiStatus': apiStatus,
    };
  }
}
